import { combineReducers } from 'redux';

export const qiblaReducer = (state = null, action) => {
    switch (action.type) {
        case 'SET_QIBLA_DIRECTION':
            return {
                ...state,
                state: action.payload,
            };
        default:
            return state;
    }
};

export const schoolReducer = (state = "", action) => {
    switch (action.type) {
        case 'SET_SCHOOL':
            return {
                ...state,
                state: action.payload,
            };
        default:
            return state;
    }
};

export const currentPrayerReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_CURRENT_PRAYER':
            return {
                ...state,
                state: action.payload,
            };
        default:
            return state;
    }
};

export const nextPrayerReducer = (state = {}, action) => {
    switch (action.type) {
        case 'SET_NEXT_PRAYER':
            return {
                ...state,
                state: action.payload,
            };
        default:
            return state;
    }
};

export const prayerTimesLoadingReducer = (state = false, action) => {
    switch (action.type) {
        case 'SET_PRAYER_TIMES_LOADING':
            return {
                ...state,
                state: action.payload,
            };
        default:
            return state;
    }
};


export default combineReducers({
  school: schoolReducer,
  qiblaDirection: qiblaReducer,
  prayerTimesLoading: prayerTimesLoadingReducer,
  currentPrayer: currentPrayerReducer,
  nextPrayer: nextPrayerReducer
});