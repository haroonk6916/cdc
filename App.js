import React, { Component } from "react";
import MuslimApp from "./MuslimApp"
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import rootReducer from './Reducers/RootReducer';

const store = createStore(rootReducer);

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <MuslimApp/>
      </Provider>
    )
  }
  }