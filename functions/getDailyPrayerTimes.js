import axios from "axios";
import PrayerManager from "./calculations"

export const getDailyPrayerTimes = async (latitude,longitude, school) => {
let prayTimes = new PrayerManager('ISNA')
let lat = 52.442669
let lng = -1.847170
let times = prayTimes.getTimes(new Date(), [lat, lng], 0) // Get prayers times for "today" at lat: 43, long: -80 with -5 timezone
// console.log('fajr : ', times.find(t => t.name === "sunrise"))
// OR

  let newDate = new Date()
let date = newDate.getDate();
const offset = new Date().getTimezoneOffset();
let month = newDate.getMonth() + 1;
let year = newDate.getFullYear();
let separator = "-"

const formattedCurrentDate = `${date}${separator}${month<10?`0${month}`:`${month}`}${separator}${year}`

  try{

  // const url = `https://api.pray.zone/v2/times/today.json?longitude=${longitude}&latitude=${latitude}&elevation=0&school=1`
    // const PrayerTimes = await axios
    //             .get(url)
    let times = prayTimes.getTimes(new Date(), [latitude, longitude], 0, school) // Get prayers times for "today" at lat: 43, long: -80 with -5 timezone

return times
  } catch (err) {
    console.log(err)
    }

  }
  
 