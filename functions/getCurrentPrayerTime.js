  // export const getCurrentPrayerTime = (prayerTimes) => {

  //   const arrayTimes = [];
  //   const currentDate = new Date(); // for now
  //   const currentHour = currentDate.getHours();
  //   const currentMinute = currentDate.getMinutes();
  //   let prayerTime = null

  //   const currentTime = `${
  //     currentHour < 10 ? "0" + currentHour.toString() : currentHour.toString()
  //   }:${
  //     currentMinute < 10
  //       ? "0" + currentMinute.toString()
  //       : currentMinute.toString()
  //   }`;
  //   let currentTimeObj = {};
  //   arrayTimes.push({ name: "current", formatted: currentTime });

  //   prayerTimes.map((prayer) => {
  //     const PrayerTime = prayer;
  //     if (
  //       prayer.name != "Imsak" &&
  //       // prayer != "Sunrise" &&
  //       prayer.name != "Sunset" &&
  //       prayer.name != "Midnight"
  //     ) {
  //       const name = prayer.name;
  //       let object = {};
  //       arrayTimes.push(prayer);
  //     }
  //   });
  //   const sortedTimes = arrayTimes.sort((a, b) =>
  //     a.formatted > b.formatted ? 1 : a.formatted === b.formatted ? (a.size > b.size ? 1 : -1) : -1
  //   );
    // sortedTimes.map((time, index) => {
    //        if (time.formatted == currentTime) {
    //     prayerTime = arrayTimes[index - 1]
    //     if (arrayTimes[index - 1] != undefined) {
    //       prayerTime = arrayTimes[index  -1]
    //     } else {
    //       prayerTime = arrayTimes[6]
    //     }
    //     }
    // });
  //   return prayerTime
  // }
  
  export const getCurrentPrayerTime = (prayerTimes) => {
    const arrayTimes = [];
    const currentDate = new Date(); // for now
    const currentHour = currentDate.getHours();
    const currentMinute = currentDate.getMinutes();
    let prayerTime = null

    const currentTime = `${
      currentHour < 10 ? "0" + currentHour.toString() : currentHour.toString()
    }:${
      currentMinute < 10
        ? "0" + currentMinute.toString()
        : currentMinute.toString()
    }`;
    let currentTimeObj = {};
    arrayTimes.push({ name: "current", formatted: currentTime });

    prayerTimes.map((prayer) => {
      const PrayerTime = prayer;
      if (
        prayer.name != "Imsak" &&
        // prayer != "Sunrise" &&
        prayer.name != "Sunset" &&
        prayer.name != "Midnight"
      ) {
        const name = prayer.name;
        let object = {};
        arrayTimes.push(prayer);
      }
    });
    const sortedTimes = arrayTimes.sort((a, b) =>
      a.formatted > b.formatted ? 1 : a.formatted === b.formatted ? (a.size > b.size ? 1 : -1) : -1
    );
    sortedTimes.map((time, index) => {
      if (time.formatted == currentTime) {
        prayerTime = arrayTimes[index - 1]
        if (arrayTimes[index - 1] != undefined) {
          prayerTime = arrayTimes[index  -1]
        } else {
          prayerTime = arrayTimes[6]
        }
        }
    });
    return prayerTime
  }