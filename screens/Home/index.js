import React, { Component } from "react";
import {
  View,
  Picker,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage"
import { ActivityIndicator } from "react-native";
import Constants from "expo-constants";
import CurrentPrayerAndTime from "../../components/CurrentPrayerAndTime";
import {connect} from "react-redux"

class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentLocation: "",
      school: "",
    };
  }

  async componentDidMount() {
    const school = await AsyncStorage.getItem("school");
    this.setState({ school: school });
    const { detailedLocation } = this.props;
    if (detailedLocation[0].city != null) {
      this.setState({ currentLocation: detailedLocation[0].city });
    } else if (detailedLocation[0].district != null) {
      this.setState({ currentLocation: detailedLocation[0].district });
    } else if (detailedLocation[0].subregion != null) {
      this.setState({ currentLocation: detailedLocation[0].subregion });
    } else {
      this.setState({ currentLocation: null });
    }
  }

  async setSchool(school) {
    try {
    // await AsyncStorage.setItem("school", school);
    this.setState({ school });
    this.props.setSchool(school)
    } catch(err) {
      console.log(err)
    }
  }

  render() {
    if (this.props.prayerTimes.length > 0 && this.props.prayerTimesLoading.state == false) {
      return (
        <View style={{ paddingTop: Constants.statusBarHeight }}>
          <View style={{ marginLeft: 20, flexDirection: "row",borderBottomColor:"grey", borderBottomWidth:2, width:120,
}}>

            <Picker
              selectedValue={this.props.school.state}
              style={{ height: 50, width: 130, paddingLeft: 20}}
              onValueChange={(itemValue, itemIndex) =>
                this.setSchool(itemValue)
              }
            >
              <Picker.Item label="Hanafi" value="Hanafi" />
              <Picker.Item label="Shafi" value="Shafi" />
            </Picker>
          </View>
          <CurrentPrayerAndTime
            prayerTimes={this.props.prayerTimes}
            currentLocation={this.state.currentLocation}
          />
        </View>
      );
    } else {
     return (    

          <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}>
                <ActivityIndicator size="large" animating={true} color="tomato"  />

        </View>
      
      )
    }
  }
}

const mapStateToProps = (state) => {
  const { school,prayerTimesLoading } = state
  return { school,prayerTimesLoading }
};


const mapDispatchToProps = (dispatch) => {
    return {
        setSchool: (payload) => {
            dispatch({
                type: 'SET_SCHOOL',
                payload,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);