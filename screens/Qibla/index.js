import React, { useState, useEffect } from 'react';
import { View, Text, Dimensions, Modal, Vibration } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { Magnetometer } from 'expo-sensors';
import { FontAwesome5 } from '@expo/vector-icons';
import LPF from "lpf";
import CompassCalibrationModal from "../../components/CompassCalibrateModal"
import {useSelector} from "react-redux"

const { height, width } = Dimensions.get('window');
export default QiblaScreen = (props) => {
  const [subscription, setSubscription] = useState(null);
  const [magnetometer, setMagnetometer] = useState(0);
  const qiblaDirection = useSelector((state) => state.qiblaDirection.state)
  const roundedQiblaDirection = Math.round(qiblaDirection)
  useEffect(() => {
    _toggle();
    LPF.init([]);
    LPF.smoothing = 0.5;
    return () => {
      _unsubscribe();
    };
  }, []);

  const _toggle = () => {
    if (subscription) {
      _unsubscribe();
    } else {
      _subscribe();
    }
  };

  const _subscribe = () => {
    setSubscription(
      Magnetometer.addListener((data) => {
        setMagnetometer(_angle(data));
      })
    );
  };

  const _unsubscribe = () => {
    subscription && subscription.remove();
    setSubscription(null);
  };

  const _angle = (magnetometer) => {
    let angle
    if (magnetometer) {
      let { x, y, z } = magnetometer;
      if (Math.atan2(y, x) >= 0) {
        angle = LPF.next(Math.atan2(y, x) * (180 / Math.PI));
      }
      else {
        angle = LPF.next((Math.atan2(y, x) + 2 * Math.PI) * (180 / Math.PI));
      }
    }

    return Math.round(angle);
  };

  const _direction = (degree) => {
    if (degree >= qiblaDirection - 5 && degree < qiblaDirection + 5) { 
      return <FontAwesome5 name="kaaba" size={24} color="white" />
    }
    else {
      return <FontAwesome5 name="kaaba" size={24} color="gray" />
    }
  };

  const _degree = (magnetometer) => {
    return magnetometer - 90 >= 0 ? magnetometer - 90 : magnetometer + 271;
  };

  return (
    <Grid style={{ backgroundColor: 'black' }}>
    <CompassCalibrationModal success={qiblaDirection != null}/>
      <Row style={{ alignItems: 'center' }} size={.9}>
        <Col style={{ alignItems: 'center' }}>
          <Text
            style={{
              color: '#fff',
              fontSize: height / 26,
              fontWeight: 'bold'
            }}>
            {_direction(_degree(magnetometer))}
          </Text>
        </Col>
      </Row>
      <Row style={{ alignItems: 'center' }} size={.1}>
        <Col style={{ alignItems: 'center' }}>
          <View style={{ position: 'absolute', width: width, alignItems: 'center', top: 0 }}>
            <View style={{
              backgroundColor:"#fff",
              height: height / 26,
              resizeMode: 'contain'
            }} />
          </View>
        </Col>
      </Row>

      <Row style={{ alignItems: 'center' }} size={2}>

        <Col style={{ alignItems: 'center' }}>

          <View style={{
            height: 10,
            backgroundColor:"red",
            justifyContent: 'center',
            alignItems: 'center',
            width:height / 4,
            borderRightColor:"#fff",
            borderRightWidth: height / 8,
            resizeMode: 'contain',
            borderBottomColor: "red",
            transform: [{ rotate: qiblaDirection - magnetometer + 'deg' }]
          }} />
        </Col>

      </Row>
      <Row>
      <Col>
        <Text style={{
          color: '#fff',
          fontSize: height / 27,
          width: width,
          position: 'absolute',
          textAlign: 'center'
        }}>
        {roundedQiblaDirection}°
          </Text>
      </Col>
      </Row>
    </Grid>

  );
}
