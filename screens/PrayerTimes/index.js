import React, { Component } from "react";
import { View } from "react-native";
import PrayerTimes from "../../components/PrayerTimes";
import { connect } from 'react-redux';
import { ActivityIndicator } from "react-native";

class PrayerTimesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      longitude: 0,
      latitude: 0,
      prayerTimes:[]
    };
  }

  render() {
    if (this.props.prayerTimes.length > 0 && this.props.prayerTimesLoading.state == false) {
      return (
        <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}>
          <PrayerTimes
            prayerTimes={this.props.prayerTimes}
          />
        </View>
      );
    } else {
      return (    

          <View
          style={{
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}>
                <ActivityIndicator size="large" animating={true} color="tomato"  />

        </View>
      
      )
    }
  }
}

const mapStateToProps = (state) => {
  const { prayerTimesLoading } = state
  return { prayerTimesLoading }
};

export default connect(mapStateToProps)(PrayerTimesScreen);
