import React, { Component } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, Dimensions } from "react-native";
import { Card, Title } from 'react-native-paper';
class CompassCalibrationModal extends Component {
  state = {
    modalVisible:true
  };

  setModalVisible = (visible) => {
    this.setState({ modalVisible: visible });
  }

  render() {
    const { modalVisible } = this.state;
    const screenWidth = Dimensions.get("window").width; //full width
    const screenHeight = Dimensions.get("window").height; //full height
if (this.props.success) {

    return (
      <View>
      
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
          
            <View style={[{width:screenWidth / 1.1}, {height:screenHeight / 3}]}>
  <Card>
         <Card.Content>
      <Title>Calibrate device</Title>
    </Card.Content>
    <Card.Cover style={{backgroundColor:"none"}} source={require('../assets/calibrate-icon.png')} />
       <Card.Content>
      
      <Text style={{textAlign:"center"}}>
Make sure your device is not near any metal objects. Calibrate your device by waving your phone in an 8 figure motion.
</Text>
    </Card.Content>
    <Card.Actions>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => this.setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Close</Text>
              </Pressable>

    </Card.Actions>
  </Card>
            </View>
          </View>
        </Modal>

      </View>
    );
}
else {return(      <View>
      
        <Modal
          animationType="slide"
          transparent={true}
          visible={modalVisible}
          onRequestClose={() => {
            Alert.alert("Modal has been closed.");
            this.setModalVisible(!modalVisible);
          }}
        >
          <View style={styles.centeredView}>
          
            <View style={[{width:screenWidth / 1.1}, {height:screenHeight / 3}]}>
  <Card>
         <Card.Content>
      <Title>Unable to retrieve qibla direction</Title>
    </Card.Content>

       <Card.Content>
      
      <Text>
      Please make sure you are connected to the internet and try again.
</Text>
    </Card.Content>
    <Card.Actions>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => this.setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Close</Text>
              </Pressable>

    </Card.Actions>
  </Card>
            </View>
          </View>
        </Modal>

      </View>)}
}
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    backgroundColor: 'rgba(100,100,100, 0.5)',
    marginBottom:48

  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
    marginLeft:5
  },
  buttonClose: {
    backgroundColor: "tomato",
  },
  textStyle: {
    color: "#fff",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default CompassCalibrationModal;