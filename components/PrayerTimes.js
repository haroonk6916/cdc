import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Picker,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage"
import NextPrayer from "./NextPrayerTime";
import {connect} from "react-redux"
import Constants from 'expo-constants';
import { getCurrentPrayerTime } from "../functions/getCurrentPrayerTime";


class PrayerTimes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prayer: [],
      school:"",
      currentPrayer:{}
    };
  }

  async setSchool(school) {
    this.props.setSchool(school)
  }
  
  async componentDidMount() {
    const school = await AsyncStorage.getItem("school")
    this.setState({school:school})
    await this.setPrayers()
  }
  
    async componentDidUpdate(prevProps, prevState) {
    const school = await AsyncStorage.getItem("school")
    if (prevProps.school != this.props.school) {
      this.setState({school:this.props.school.state})
    }
    
    if(prevProps.prayerTimes != this.props.prayerTimes) {
      await this.setPrayers()
    }

    if(prevProps.currentPrayer != this.props.currentPrayer) {
      await this.setPrayers()
    }
  }
  
  async setPrayers() {
    const currentPrayer = getCurrentPrayerTime(this.props.prayerTimes);
    this.setState({ currentPrayer: currentPrayer });
  }


  render() {
    const screenWidth = Dimensions.get("window").width; //full width
    const screenHeight = Dimensions.get("window").height; //full height

    return (
      <View style={{paddingTop:Constants.statusBarHeight, height:screenHeight}}>
        <View
          style={{
            height: (screenHeight / 10) * 4,
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <NextPrayer prayerTimes={this.props.prayerTimes} />
        </View>
        <View
          style={{
            marginTop:(screenHeight/10) * 1,
            marginBottom:20,
            height:  (screenHeight / 10) * 4,
            justifyContent: "center",
            alignItems: "center",
            elevation: 1,
            backgroundColor: "#fff",
            borderRadius: 3,
          }}
        >
          <View
            style={{
              width: screenWidth / 1.1,
              margin: 6,
              borderColor: "#E3E3E3",
              textAlign: "center",
              justifyContent: "center",
              flex: 1,
            }}
          >
            <Picker
              selectedValue={this.props.school.state}
              style={{ height: 50, width: 150, fontSize: 60 }}
              onValueChange={(itemValue, itemIndex) =>
                this.setSchool(itemValue)
              }
            >
              <Picker.Item label="Hanafi" value="Hanafi" />
              <Picker.Item label="Shafi" value="Shafi" />
            </Picker>
          </View>
          {this.props.prayerTimes.map((salah) =>
            salah.name != "Imsak" && salah.name != "Midnight" && salah.name != "Sunset" ? (
              <View
                style={{
                  width: screenWidth / 1.1,
                  margin: 6,
                  borderColor: "#E3E3E3",
                  borderBottomWidth: 1,
                  textAlign: "center",
                  justifyContent: "center",
                  flex: 1,
                }}
              >
              {salah.formatted == this.state.currentPrayer.formatted ?
              (
        <View style={styles.container}>
                  <View style={[styles.subContainer]}>
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.currentPrayerTextStyle}>{salah.name}</Text>
                      <Text style={[styles.currentPrayerTextStyle, { textAlign: "right" }]}>
                        {salah.formatted}
                      </Text>
                    </View>
                  </View>
                </View>
              ): (
                <View style={styles.container}>
                  <View style={[styles.subContainer]}>
                    <View style={{ flexDirection: "row" }}>
                      <Text style={styles.textStyle}>{salah.name}</Text>
                      <Text style={[styles.textStyle, { textAlign: "right" }]}>
                        {salah.formatted}
                      </Text>
                    </View>
                  </View>
                </View>
              )}
              </View>
            ) : (
              <View />
            )
          )}
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  subContainer: {
    alignSelf: "stretch",
    height: 20,
    marginLeft: 20,
    marginRight: 20,
  },
    currentPrayerTextStyle: {
    fontSize: 18,
    fontWeight: "700",
    color: "black",
    flex: 1,
  },
  textStyle: {
    fontSize: 18,
    fontWeight: "700",
    color: "grey",
    flex: 1,
  },
});

const mapStateToProps = (state) => {
  const { school, currentPrayer } = state
  return { school, currentPrayer }
};


const mapDispatchToProps = (dispatch) => {
    return {
        setSchool: (payload) => {
            dispatch({
                type: 'SET_SCHOOL',
                payload,
            });
        },
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PrayerTimes);
