import React, { Component } from "react";
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  ActivityIndicator,
} from "react-native";
import { getCurrentPrayerTime } from "../functions/getCurrentPrayerTime";
import { getNextPrayerTime } from "../functions/getNextPrayerTime";
import { Entypo } from "@expo/vector-icons";
import {connect} from "react-redux"

class CurrentPrayerAndTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prayer: [],
      currentTime: 0,
      nextPrayer: {},
      currentPrayer: {},
      school: "",
    };

    this.daysArray = [
      "sunday",
      "monday",
      "tuesday",
      "wednesday",
      "thursday",
      "friday",
      "saturday",
      "sunday",
    ];
  }

  componentDidMount() {
    this.setPrayers();
    setInterval(() => {
      const currentDate = new Date().toLocaleString();
      const currentDay = new Date().getDay();
      const currentTime = new Date(currentDate).toLocaleTimeString("en");
      this.setState({
        currentTime: currentTime,
      });
      this.setPrayers()
    }, 1000);


    this.daysArray.map((item, key) => {
      if (key == new Date().getDay()) {
        this.setState({ currentDay: item.toUpperCase() });
      }
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if(prevProps.prayerTimes != this.props.prayerTimes) {
      await this.setPrayers()
    }
  }

  setPrayers() {
    const nextPrayer = getNextPrayerTime(this.props.prayerTimes);
    const currentPrayer = getCurrentPrayerTime(this.props.prayerTimes);
    this.setState({ nextPrayer: nextPrayer, currentPrayer: currentPrayer });
    if (currentPrayer != this.props.currentPrayer.state) {
    this.props.setCurrentPrayer(currentPrayer)
    this.props.setNextPrayer(nextPrayer)
    }
  }

  render() {
    const screenWidth = Dimensions.get("window").width; //full width
    const screenHeight = Dimensions.get("window").height; //full width
    if (this.state.currentTime != 0) {
      return (
        <View style={styles.container}>
          <View>
            {this.props.currentLocation != null && this.props.currentLocation.length > 0 ? (
              <Text style={styles.locationText}>
                <Entypo name="location-pin" size={30} color="tomato" />
                {this.props.currentLocation}
               </Text>
            ) : null}
            <View style={styles.dateAndTimes}>
              <Text style={styles.daysText}>{this.state.currentDay}</Text>
              <Text style={styles.timeText}>{this.state.currentTime}</Text>
              <Text style={styles.currentPrayerText}>
                {" "}
                Now - {this.state.currentPrayer.name}:{" "}
                {this.state.currentPrayer.formatted}
              </Text>
              <Text style={styles.nextPrayerText}>
                {" "}
                Next - {this.state.nextPrayer.name}:{" "}
                {this.state.nextPrayer.formatted}
              </Text>
            </View>
          </View>
        </View>
      );
    } else {
      return (
        <View style={styles.spinnerContainer}>
          <ActivityIndicator size="large" animating={true} color="tomato" />
        </View>
      );
    }
  }
}
const mapDispatchToProps = (dispatch) => {
    return {
        setCurrentPrayer: (payload) => {
            dispatch({
                type: 'SET_CURRENT_PRAYER',
                payload,
            });
        },
        setNextPrayer: (payload) => {
            dispatch({
                type: 'SET_NEXT_PRAYER',
                payload,
            });
        },
    };
};

const mapStateToProps = (state) => {
  const {currentPrayer} = state
  return{
    currentPrayer
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrentPrayerAndTime)

const screenWidth = Dimensions.get("window").width; //full width
const screenHeight = Dimensions.get("window").height; //full width
const styles = StyleSheet.create({
  container: {
    textAlign: "center",
    width: screenWidth,
    alignItems: "center",
    justifyContent: "center",
    marginTop:20
  },
  spinnerContainer: {
    width: screenWidth,
    height: screenHeight,
    alignItems: "center",
    justifyContent: "center",
  },
  dateAndTimes: {
    paddingTop: screenHeight / 5,
  },
  timeText: {
    fontSize: 50,
    color: "#000",
    marginBottom: 5,
    textAlign: "center",
  },
  daysText: {
    textAlign: "center",
    color: "#2196f3",
    fontSize: 20,
    paddingBottom: 0,
    fontWeight: "bold",
    marginBottom: 5,
  },
  locationText: {
    color: "#2196f3",
    fontSize: 30,
    paddingBottom: 20,
    textAlign: "center",
  },
  currentPrayerText: {
    textAlign: "center",

    marginTop: 20,
    color: "black",
    fontSize: 20,
    fontWeight: "bold",
  },
  nextPrayerText: {
    textAlign: "center",

    marginTop: 20,
    color: "grey",
    fontSize: 20,
    fontWeight: "bold",
  },
});
