import React, { Component } from "react";
import { View, Dimensions, StyleSheet  } from "react-native";
import { Card, Title} from 'react-native-paper';
import Constants from 'expo-constants';
import {connect} from "react-redux"

import {getNextPrayerTime} from '../functions/getNextPrayerTime'
import Image1 from "../assets/izuddin-helmi-adnan-onh-FdFUyeM-unsplash.jpg"
import Image2 from "../assets/juan-camilo-guarin-p-BCF7cHvc778-unsplash.jpg"
import Image3 from "../assets/katerina-kerdi-TAfqq1B3-2s-unsplash.jpg"
import Image4 from "../assets/nurhan-yC70QqvrPRk-unsplash.jpg"


class NextPrayerTime extends Component {
  constructor(props) {
    super(props);
    this.state = {
      prayer: [],
      nextPrayerTime: "",
      currentImage: ""
    };
  }
  
  componentDidMount() {
    const nextPrayerTime = getNextPrayerTime(this.props.prayerTimes);
    this.setState({nextPrayerTime: nextPrayerTime})
    this.getRandomImageFromAssets()
  }

  componentDidUpdate(prevProps) {
    if (this.props.nextPrayer.state.formatted != this.state.nextPrayerTime.formatted) {
      const nextPrayerTime = getNextPrayerTime(this.props.prayerTimes)
      this.setState({nextPrayerTime: nextPrayerTime})
    }
  }

  getRandomImageFromAssets() {
    const images= [
  Image1,
  Image2,
  Image3,
  Image4,
  ];
   const randomNumber = Math.floor(Math.random() * images.length);
    this.setState({
      currentImage: images[randomNumber]
    });
  }

  render() {
    
    const screenWidth = Dimensions.get("window").width; //full width
    const screenHeight = Dimensions.get("window").height; //full width

    return (
<View style={{width:screenWidth,position:"absolute", top:0,paddingLeft: 10,
  paddingRight: 10, paddingTop: Constants.statusBarHeight}}>
<Card style={{
  }}>
    <Card.Cover source={this.state.currentImage} />
    <Card.Actions>
      <Title>{this.state.nextPrayerTime.name}: {this.state.nextPrayerTime.formatted}</Title>
    </Card.Actions>
  </Card>
</View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: Constants.statusBarHeight
  },
  logo: {
    width: 66,
    height: 58,
  },
});


const mapStateToProps = (state) => {
  const { nextPrayer } = state
  return { nextPrayer }
};


export default connect(mapStateToProps)(NextPrayerTime);
