import React, { Component } from "react";
import { Text, View, StyleSheet, Alert, Linking, Button } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import Constants from "expo-constants";
import { NavigationContainer } from "@react-navigation/native";
import "react-native-gesture-handler";
import HomeScreen from "./screens/Home";
import PrayerTimesScreen from "./screens/PrayerTimes";
import QiblaScreen from "./screens/Qibla";
import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { getDailyPrayerTimes } from "./functions/getDailyPrayerTimes";
import { getQiblaDirection } from "./functions/getQiblaDirection";
import { ActivityIndicator } from "react-native";
import * as Location from "expo-location";
import { connect } from "react-redux";
import * as ScreenOrientation from 'expo-screen-orientation';

class MuslimApp extends Component {
  _unsubscribe = null;
  constructor(props) {
    super(props);
    this.state = {
      latitude: 0,
      longitude: 0,
      prayerTimes: {},
      detailedLocation: [],
      school: "",
      loading: false,
      count: 0,
      isConnected: true,
      locationAllowed: true,
    };
  }
  async componentDidMount() {
  await ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT_UP);
    await this.checkSchool();
    this.setData();
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.school != this.props.school) {
      this.setData();
    }
  }
  componentWillUnmount() {
    if (this.unsubscribe != null) this.unsubscribe();
  }
  async checkSchool() {
    try {
      const school = await AsyncStorage.getItem("school");
      if (school !== null) {
        this.setState({ school: school });
        this.props.setSchool(school);
      } else {
        await AsyncStorage.setItem("school", "Shafi");
        this.setState({ school: "Shafi" });
        this.props.setSchool("Shafi");
      }
    } catch (error) {
      console.log(error);
    }
  }

  async setGlobalPrayerTimes(latitude, longitude, school) {
    try {
      await getDailyPrayerTimes(latitude, longitude, school).then(
        (prayerTimes) => {
          if (prayerTimes.length > 0) {
            this.setState({
              prayerTimes: prayerTimes,
            });
          } else {
            this.setState({ error: "Unexpected error occurred." });
          }
          this.props.setPrayerTimesLoading(false);
        }
      );

      const qiblaDirection = await getQiblaDirection(latitude, longitude);
      if (qiblaDirection) {
        this.props.setQiblaDirection(qiblaDirection);
      }
    } catch (err) {
      console.log(err);
    }
  }

  async setData() {
    const school = this.props.school.state;
    await AsyncStorage.setItem("school", school);
    navigator.geolocation.getCurrentPosition(
  location => {
      const latitude = location.coords.latitude;
      const longitude = location.coords.longitude;
      this.setGlobalPrayerTimes(latitude, longitude, school);
      this.setState({
        school: school,
      });
      const detailedLocation = Location.reverseGeocodeAsync({
        latitude,
        longitude,
      });
      this.setState({
        detailedLocation: detailedLocation,
      });
  },
  error => Alert.alert(error.message),
  { enableHighAccuracy: false, timeout: 10000, maximumAge: 1000 }
);
  }

  render() {
    const Stack = createBottomTabNavigator();
    if (this.state.prayerTimes.length > 0) {
      return (
        <NavigationContainer>
          <Stack.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === "Home") {
                  iconName = "home-outline";
                } else if (route.name === "Prayer Times") {
                  iconName = "time-outline";
                } else if (route.name === "Qibla") {
                  iconName = "compass-outline";
                } else if (route.name === "Todo List") {
                  iconName = "list-outline";
                }

                return <Ionicons name={iconName} size={size} color={color} />;
              },
            })}
            tabBarOptions={{
              activeTintColor: "tomato",
              inactiveTintColor: "gray",
            }}
          >
            <Stack.Screen name="Home">
              {(props) => (
                <HomeScreen
                  {...props}
                  prayerTimes={this.state.prayerTimes}
                  detailedLocation={this.state.detailedLocation}
                />
              )}
            </Stack.Screen>
            <Stack.Screen name="Prayer Times">
              {(props) => (
                <PrayerTimesScreen
                  {...props}
                  prayerTimes={this.state.prayerTimes}
                />
              )}
            </Stack.Screen>
            <Stack.Screen name="Qibla" component={QiblaScreen} />
          </Stack.Navigator>
        </NavigationContainer>
      );
    } else if (!this.state.locationAllowed) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View>
            <Text
              style={{ fontSize: 30, paddingBottom: 20, textAlign: "center" }}
            >
              Permission Required
            </Text>
            <Text
              style={{ margin: 20, fontWeight: "bold", textAlign: "center" }}
            >
              Salam, we require Location permission in order to give you
              accurate prayer times. To continue using this app, you can
              <Text
                style={{ color: "blue" }}
                onPress={() => Linking.openSettings()}
              >
                {" "}
                open settings here.
              </Text>
              From here, click Permissions > Location. Select the option you
              like and restart the app.
            </Text>
          </View>
          <Button
            title="Open Settings"
            onPress={() => Linking.openSettings()}
          />
        </View>
      );
    } else {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View style={{ marginBottom: 50 }}></View>
          <ActivityIndicator size="large" animating={true} color="tomato" />
        </View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    paddingTop: Constants.statusBarHeight,
    backgroundColor: "#ecf0f1",
    padding: 8,
  },
});
const mapStateToProps = (state) => {
  const { school, prayerTimesLoading } = state;
  return { school, prayerTimesLoading };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setSchool: (payload) => {
      dispatch({
        type: "SET_SCHOOL",
        payload,
      });
    },
    setQiblaDirection: (payload) => {
      dispatch({
        type: "SET_QIBLA_DIRECTION",
        payload,
      });
    },
    setPrayerTimesLoading: (payload) => {
      dispatch({
        type: "SET_PRAYER_TIMES_LOADING",
        payload,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MuslimApp);
